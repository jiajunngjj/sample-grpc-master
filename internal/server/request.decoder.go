package server

import (
	"context"
	"git.bluebird.id/fajri.rahmat/sample-grpc/proto"
	"git.bluebird.id/fajri.rahmat/sample-grpc/service"
)

func decodeHelloGrpcRequest(ctx context.Context, request interface{}) (interface{}, error) {
	req := request.(*proto.HelloRequest)
	return service.HelloRequest{
        Message: req.Message,
    }, nil
}

