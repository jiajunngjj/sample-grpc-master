package server

import (
	"context"
	"fmt"
	"log"
	"os"
	"os/signal"
	"strconv"
	"syscall"

	"git.bluebird.id/fajri.rahmat/sample-grpc/internal/endpoint"
	"git.bluebird.id/fajri.rahmat/sample-grpc/proto"
	"git.bluebird.id/fajri.rahmat/sample-grpc/repository"
	"git.bluebird.id/fajri.rahmat/sample-grpc/service"
	"git.bluebird.id/logistic/commons/logger"

	apmgrpc "git.bluebird.id/lib/apm/grpc"
	gt "github.com/go-kit/kit/transport/grpc"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/reflection"
)

type Runner interface {
	proto.HelloServer
	proto.HealthServer
	Run()
}

type server struct {
	hello gt.Handler
	close func()
	proto.UnimplementedHelloServer
	proto.UnimplementedHealthServer
}

func New() (Runner, error) {
	db, err := repository.NewSQLiteDataSource("file:db.sqlite")
	if err != nil {
		return nil, err
	}
	svc, err := service.NewHelloService(db)
	if err != nil {
		return nil, err
	}
	opts := []gt.ServerOption{gt.ServerBefore(trackBefore)}
	return &server{
		hello: gt.NewServer(
			endpoint.MakeHelloEndpoint(svc),
			decodeHelloGrpcRequest,
			encodeHelloGrpcResponse,
			opts...,
		),
		close: func() {
			db.Close()
		},
	}, nil
}

func trackBefore(ctx context.Context, md metadata.MD) context.Context {
	logger.Info("Request incoming")
	return ctx
}
func (g *server) HelloWorld(ctx context.Context, request *proto.HelloRequest) (*proto.HelloResponse, error) {
	logger.Info(fmt.Sprintf("someone is greeting you with message %s", request.Message))
	_, resp, err := g.hello.ServeGRPC(ctx, request)
	if err != nil {
		return nil, err
	}
	return resp.(*proto.HelloResponse), nil
}

func (g *server) Check(_ context.Context, req *proto.HealthCheckRequest) (*proto.HealthCheckResponse, error) {
	logger.Info("requesting Health check")
	return &proto.HealthCheckResponse{
		Status: proto.HealthCheckResponse_SERVING,
	}, nil
}
func (g *server) Watch(req *proto.HealthCheckRequest, s proto.Health_WatchServer) error {
	for {
		if err := s.Send(&proto.HealthCheckResponse{
			Status: proto.HealthCheckResponse_SERVING,
		}); err != nil {
			logger.Error(err.Error())
			return err
		}
	}
}

func (g *server) Run() {
	errs := make(chan error)
	go func() {
		c := make(chan os.Signal)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM, syscall.SIGALRM)
		errs <- fmt.Errorf("%s", <-c)
	}()
	var server *grpc.Server
	if creds == nil {
		server = grpc.NewServer(apmgrpc.GetElasticAPMServerOptions()...)
	} else {
		server = grpc.NewServer(append(apmgrpc.GetElasticAPMServerOptions(), grpc.Creds(creds))...)
	}
	proto.RegisterHelloServer(server, g)
	proto.RegisterHealthServer(server, g)
	reflection.Register(server)
	go func() {
		errs <- serve(server, healthcheck())
	}()
	defer server.GracefulStop()
	defer g.close()
	log.Fatalf("Stop server with error detail: %v", <-errs)
}

func getServicePort() int {
	port := os.Getenv("SERVICE_PORT")
	if port == "" {
		return 8080
	} else {
		p, err := strconv.Atoi(port)
		if err != nil {
			return 8080
		}
		return p
	}
}
