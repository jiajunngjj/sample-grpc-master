package server

import (
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"os"
	"strings"
	"time"
	"github.com/soheilhy/cmux"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

const (
	defaultServiceIP   = "127.0.0.1"
	defaultServicePort = "8080"
)

var creds credentials.TransportCredentials
var cert tls.Certificate

func init() {
	cert, creds, _ = getCert()
}

func serve(srv *grpc.Server, other http.Handler) error {
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", getServicePort()))
	if err != nil {
		return err
	}
	if creds == nil {
		m := cmux.New(lis)
		grpcl := m.Match(cmux.HTTP2(), cmux.HTTP2HeaderFieldPrefix("content-type", "application/grpc"))
		httpl := m.Match(cmux.HTTP1Fast())
		log.Printf("Server run on :%d", getServicePort())
		https := &http.Server{
			Handler:      other,
			ReadTimeout:  time.Duration(30 * time.Second),
			WriteTimeout: time.Duration(30 * time.Second),
		}
		go srv.Serve(grpcl)
		go https.Serve(httpl)
		return m.Serve()
	} else {
		httpServer := &http.Server{
			Addr:    fmt.Sprintf(":%d", getServicePort()),
			Handler: routeHandler(srv, other),
			TLSConfig: &tls.Config{
				NextProtos:   []string{"h2"},
				Certificates: []tls.Certificate{cert},
			},
			ReadTimeout:  time.Duration(30 * time.Second),
			WriteTimeout: time.Duration(30 * time.Second),
		}
		log.Printf("Server run on :%d", getServicePort())
		return httpServer.Serve(tls.NewListener(lis, httpServer.TLSConfig))
	}
}

func getCertPath() (ca, server, key string) {
	ca = os.Getenv("CACERT_PATH")
	server = os.Getenv("SERVERCERT_PATH")
	key = os.Getenv("KEYCERT_PATH")
	return
}

func getServiceIP() string {
	ip := os.Getenv("SERVICE_IP")
	if ip == "" {
		ip = defaultServiceIP
	}
	return ip
}

func getCert() (tlsCert tls.Certificate, creds credentials.TransportCredentials, err error) {
	ca, server, key := getCertPath()
	if len(ca) > 0 && len(server) > 0 && len(key) > 0 {
		tlsCert, err = tls.LoadX509KeyPair(server, key)
		if err != nil {
			log.Println(err.Error())
			return
		}
		creds, err = tlsCredentialFromKeyPair(ca, tlsCert, true)
		if err != nil {
			log.Println(err.Error())
			return
		}
	}
	return
}

func tlsCredentialFromKeyPair(cacert string, cert tls.Certificate, mutual bool) (credentials.TransportCredentials, error) {

	rawCaCert, err := ioutil.ReadFile(cacert)
	if err != nil {
		return nil, err
	}

	return tlsCredential(rawCaCert, cert, mutual), nil
}

func tlsCredential(cacert []byte, cert tls.Certificate, mutual bool) credentials.TransportCredentials {
	caCertPool := x509.NewCertPool()
	caCertPool.AppendCertsFromPEM(cacert)

	tlsCfg := &tls.Config{
		Certificates: []tls.Certificate{cert},
		ClientCAs:    caCertPool,
		RootCAs:      caCertPool,
	}

	if mutual {
		tlsCfg.ClientAuth = tls.RequireAndVerifyClientCert
	}

	return credentials.NewTLS(tlsCfg)
}

func routeHandler(grpc http.Handler, other http.Handler) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		if r.ProtoMajor == 2 && strings.HasPrefix(
			r.Header.Get("Content-Type"), "application/grpc") {
			grpc.ServeHTTP(rw, r)
		} else {
			other.ServeHTTP(rw, r)
		}
	})
}

func healthcheck() http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {fmt.Fprintf(rw, `{"status": "UP"}`)
})
}