package server

import (
	"context"
	"git.bluebird.id/fajri.rahmat/sample-grpc/proto"
	"git.bluebird.id/fajri.rahmat/sample-grpc/service"
)

func encodeHelloGrpcResponse(ctx context.Context, response interface{}) (interface{}, error) {
	resp := response.(service.HelloResponse)
	return &proto.HelloResponse{
		Message: resp.Message,
	}, nil
}
