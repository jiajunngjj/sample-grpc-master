package endpoint

import (
	"context"

	"git.bluebird.id/fajri.rahmat/sample-grpc/service"

	"github.com/go-kit/kit/endpoint"
)

//MakeHelloEndpoint ...
func MakeHelloEndpoint(svc service.Hello) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(service.HelloRequest)
		resp := svc.SayHello(req)
		return resp, nil
	}
}
