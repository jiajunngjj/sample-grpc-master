package service

//HelloRequest ...
type HelloRequest struct {
	Message string
}

//HelloResponse ...
type HelloResponse struct {
	Message string
}
