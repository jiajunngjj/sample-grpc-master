package service

import (
	"git.bluebird.id/fajri.rahmat/sample-grpc/repository"
)

//Hello ...
type Hello interface {
	SayHello(HelloRequest) HelloResponse
}

type helloService struct {
	dbrw repository.DBReaderWriter
}

func NewHelloService(dbrw repository.DBReaderWriter) (Hello, error) {
	return &helloService{
		dbrw,
	}, nil
}

func (h *helloService) SayHello(request HelloRequest) HelloResponse {
	msg, err := h.dbrw.GetHello(request.Message)
	if err != nil {
		return HelloResponse{
			Message: err.Error(),
		}
	}
	return HelloResponse{
		Message: msg,
	}
}
