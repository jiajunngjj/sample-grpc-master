package model

import "time"

//HelloRequest ...
type HelloRequest struct {
	Message string
}

//HelloResponse ...
type HelloResponse struct {
	Message string
}

type HelloMessage struct {
	ID       int
	Message  string
	CretedAt time.Time
}
