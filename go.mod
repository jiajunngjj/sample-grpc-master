module git.bluebird.id/fajri.rahmat/sample-grpc

go 1.16

require (
	git.bluebird.id/lib/apm v0.0.4
	git.bluebird.id/logistic/commons v1.0.9
	github.com/go-kit/kit v0.12.0
	github.com/go-redis/cache/v7 v7.0.2
	github.com/go-redis/redis/v7 v7.4.1
	github.com/joho/godotenv v1.3.0
	github.com/mattn/go-sqlite3 v1.14.10
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/pressly/goose v2.7.0+incompatible
	github.com/soheilhy/cmux v0.1.4
	github.com/vmihailenco/msgpack/v4 v4.3.12
	go.elastic.co/apm v1.14.0 // indirect
	google.golang.org/grpc v1.41.0
	google.golang.org/grpc/examples v0.0.0-20210927213616-4555155af248 // indirect
	google.golang.org/protobuf v1.27.1
)
