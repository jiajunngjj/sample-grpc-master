FROM gcr.io/distroless/static

ADD sample-grpc ./
ADD migration ./migration
ENTRYPOINT ["./sample-grpc"]
