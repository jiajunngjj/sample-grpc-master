package migration

import (
	"database/sql"

	"github.com/pressly/goose"
)

func init() {
	goose.AddMigration(Up20220111103345, Down20220111103345)
}

func Up20220111103345(tx *sql.Tx) error {
	_, err := tx.Exec("CREATE TABLE IF NOT EXISTS messages(id INTEGER PRIMARY KEY AUTOINCREMENT, msg TEXT, created_at INTEGER);")
	return err
}

func Down20220111103345(tx *sql.Tx) error {
	_, err := tx.Exec("DROP TABLE messages;")
	return err
}
