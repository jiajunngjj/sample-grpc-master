package grpc

import (
	apmgrpc "git.bluebird.id/lib/apm/grpc"
	"git.bluebird.id/logistic/commons/cert"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

type ConnectionOption struct {
	Address               string
	IsSecure              bool
	IsMTLS                bool
	ServerCertificatePath string
	CACertificatePath     string
	KeyCertificatePath    string
	ServerNameOverride    string
}

func NewConnection(opts ConnectionOption, dialOpts ...grpc.DialOption) (*grpc.ClientConn, error) {
	var creds credentials.TransportCredentials
	var err error
	if opts.IsSecure {
		if opts.IsMTLS {
			_, credsVal, _ := cert.CreateTransportCredentials(
				opts.CACertificatePath,
				opts.ServerCertificatePath,
				opts.KeyCertificatePath,
			)
			creds = *credsVal
		} else {
			creds, err = credentials.NewClientTLSFromFile(opts.ServerCertificatePath, opts.ServerNameOverride)
			if err != nil {
				return nil, err
			}
		}
	}
	dialOptions := apmgrpc.GetElasticAPMClientOption()
	if creds == nil {
		dialOptions = append(dialOptions, grpc.WithInsecure())
	} else {
		dialOptions = append(dialOptions, grpc.WithTransportCredentials(creds))
	}
	conn, err := grpc.Dial(opts.Address, append(dialOptions, dialOpts...)...)
	if err != nil {
		return nil, err
	}
	return conn, nil
}
