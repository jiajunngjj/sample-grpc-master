package cache

import "context"

type CacheConfigOptions struct {
	TTL int
}

type Cacher interface {
	Get(ctx context.Context, key interface{}, value interface{}) error
	Set(ctx context.Context, key interface{}, value interface{}, ttl int64) error
	Delete(ctx context.Context, key interface{}) error
}
