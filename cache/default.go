package cache

import (
	"context"
	"fmt"
	"time"

	"errors"

	"git.bluebird.id/logistic/commons/config"
	"github.com/patrickmn/go-cache"
)

type localCache struct {
	db *cache.Cache
}

func DefaultLocalCache() (Cacher, error) {
	cache := cache.New(
		time.Duration(config.GetInt("CACHE_DEFAULT_TTL", 10))*time.Second,
		time.Duration(config.GetInt("CACHE_CLEAN_INTERVAL", 3600))*time.Second)
	return &localCache{
		db: cache,
	}, nil
}

func (l *localCache) Get(_ context.Context, key interface{}, value interface{}) error {
	if v, ok := l.db.Get(fmt.Sprintf("%v", key)); !ok {
		return errors.New("No cache found")
	} else {
		value = v
		return nil
	}
}
func (l *localCache) Set(_ context.Context, key interface{}, value interface{}, ttl int64) error {
	l.db.Set(fmt.Sprintf("%v", key), value, time.Duration(ttl)*time.Second)
	return nil
}
func (l *localCache) Delete(_ context.Context, key interface{}) error {
	l.db.Delete(fmt.Sprintf("%v", key))
	return nil
}
