package redis

import (
	"context"
	"fmt"
	"time"

	local "git.bluebird.id/fajri.rahmat/sample-grpc/cache"

	"git.bluebird.id/logistic/commons/config"
	"github.com/go-redis/cache/v7"
	"github.com/go-redis/redis/v7"
	"github.com/vmihailenco/msgpack/v4"
)

type redisCache struct {
	rd *cache.Codec
	cl *redis.Client
}

func New() (local.Cacher, error) {
	cl := redis.NewClient(&redis.Options{
		Addr:     config.Get("REDIS_HOST", "127.0.0.1"),
		Password: config.Get("REDIS_PASSWORD", ""),
		DB:       config.GetInt("REDIS_DATABASE", 0),
	})
	rd := &cache.Codec{
		Redis: cl,
		Marshal: func(v interface{}) ([]byte, error) {
			return msgpack.Marshal(v)
		},
		Unmarshal: func(b []byte, v interface{}) error {
			return msgpack.Unmarshal(b, v)
		},
	}
	return &redisCache{
		rd: rd,
		cl: cl,
	}, nil
}

func (r *redisCache) Get(_ context.Context, key, value interface{}) error {
	return r.rd.Get(fmt.Sprintf("%v", key), value)
}
func (r *redisCache) Set(_ context.Context, key interface{}, value interface{}, ttl int64) error {
	if ttl == 0 {
		ttl = int64(config.GetInt("REDIS_CACHE_DEFAULT_TTL", 10))
	}
	return r.rd.Set(&cache.Item{
		Key:        fmt.Sprintf("%v", key),
		Object:     value,
		Expiration: time.Duration(ttl) * time.Second,
	})
}

func (r *redisCache) Delete(ctx context.Context, key interface{}) error {
	keys, err := r.cl.Keys(fmt.Sprintf("%v", key)).Result()
	if err != nil {
		return err
	}
	return r.cl.Del(keys...).Err()
}

