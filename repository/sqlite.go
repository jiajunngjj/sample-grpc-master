package repository

import (
	"database/sql"
	"errors"
	"fmt"
	"time"

	_ "git.bluebird.id/fajri.rahmat/sample-grpc/migration"
	"git.bluebird.id/logistic/commons/logger"
	_ "github.com/mattn/go-sqlite3"
	"github.com/pressly/goose"
)

type sqliteds struct {
	db *sql.DB
}

func NewSQLiteDataSource(file string) (DBReaderWriter, error) {
	db, err := sql.Open("sqlite3", file)
	if err != nil {
		logger.Error("Error ", err.Error())
		return nil, errors.New("Failed connect to database")
	}
	db.SetMaxOpenConns(1)
	if errdb := goose.Up(db, "migration"); errdb != nil {
		logger.Error(errdb.Error())
	}
	return &sqliteds{
		db: db,
	}, nil
}

func (s *sqliteds) Close() error {
	return s.db.Close()
}

func (s *sqliteds) GetHello(msg string) (string, error) {
	// test sql injection vulnerability
	t := time.Now()
	query := fmt.Sprintf("insert into messages(msg, created_at) values ('%s',%d)", msg, t.Unix())
	_, err := s.db.Exec(query)
	if err != nil {
		logger.Error("Error: ", err.Error())
		return "", errors.New("Failed execute query statement")
	}
	return fmt.Sprintf("We got message: %s at %s", msg, t), nil
}
