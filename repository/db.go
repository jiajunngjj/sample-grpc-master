package repository

import "io"

type DBReaderWriter interface {
	io.Closer
	GetHello(msg string) (string, error)
}
