def Tag_Release() {
    sh 'git describe --tags --abbrev=0 > Tag_Release'
    def Tag_Release = readFile('Tag_Release').trim()
    sh 'rm Tag_Release'
    Tag_Release
}

pipeline {
    agent {
        node {
            label 'slave-00 || slave-01 || master'
            customWorkspace "workspace/${env.BRANCH_NAME}/src/git.bluebird.id/fajri.rahmat/sample-grpc"
        }
    }
    environment {
        SERVICE = 'sample-grpc'
        TEAMS_MICROSOFT = credentials('c5e45181-4f73-47f9-94f6-efd36e976aa5')
    }
    options {
        buildDiscarder(logRotator(daysToKeepStr: env.BRANCH_NAME == 'master' ? '90' : '30'))
    }
    stages {
        stage('Checkout') {
            when {
                anyOf { branch 'master'; branch 'develop'; branch 'staging' }
            }
            steps {
                echo 'Checking out from Git'
                checkout scm
            }
        }
        stage('Unit Testing'){
            environment{
                 GOLANG_PROTOBUF_REGISTRATION_CONFLICT= "warn"
            }
            steps {
                sh "GO111MODULE=on go test ./... -coverprofile coverage.out"
            }
        }
    
        stage('Prepare') {
            steps {
                withCredentials([file(credentialsId: '3521ab7f-3916-4e56-a41e-c0dedd2e98e9', variable: 'sa')]) {
                sh "cp $sa service-account.json"
                sh "chmod 644 service-account.json"
                sh "docker login -u _json_key --password-stdin https://asia.gcr.io < service-account.json"
                }
            }
        }
        
        stage('Build and Deploy') {
            environment {
                GOPATH = "${env.JENKINS_HOME}/workspace/${env.BRANCH_NAME}"
                PATH = "${env.GOPATH}/bin:${env.PATH}"
                VERSION_PREFIX = '1.0'
            }
            stages {
                stage('Deploy to development') {
                    when {
                        expression { return BRANCH_NAME == 'develop' || BRANCH_NAME.startsWith('feature/') || BRANCH_NAME.startsWith('bugfix/') }
                    }
                    environment {
                        ALPHA = "${env.BRANCH_NAME.split('/').size() > 1 ? env.BRANCH_NAME.split('/')[1] : env.BRANCH_NAME}-alpha.${env.BUILD_NUMBER}"
                        NAMESPACE="corporate-customer-dev-0721"
                    }
                    steps {
                        withCredentials([file(credentialsId: '3b5855ab-890b-47e9-a82a-cef6f2d683f5', variable: 'kubeconfig')]) {
                        sh "cp $kubeconfig kubeconfig.conf"
                        sh "chmod 644 kubeconfig.conf"
                        sh "gcloud auth activate-service-account --key-file service-account.json"
                        sh 'chmod +x build.sh'
                        sh './build.sh $ALPHA'
                        sh 'chmod +x deploy.sh'
                        sh './deploy.sh $ALPHA $NAMESPACE default'
                        sh 'rm kubeconfig.conf service-account.json'
                        }
                    }
                }
                stage('Deploy to staging') {
                    when {
                        expression { return BRANCH_NAME.startsWith('release/') }
                    }
                    environment {
                        BETA = "${env.BRANCH_NAME.split('/')[1]}-beta.${env.BUILD_NUMBER}"
                        NAMESPACE="bbone-dev"
                    }
                    steps {
                        withCredentials([file(credentialsId: '5266646f-abe8-4a8c-85f0-d7b1fbec0b2f', variable: 'kubeconfig')]) {
                        sh "cp $kubeconfig kubeconfig.conf"
                        sh "chmod 644 kubeconfig.conf"
                        sh "gcloud auth activate-service-account --key-file service-account.json"
                        sh 'chmod +x build.sh'
                        sh './build.sh $BETA'
                        sh 'chmod +x deploy.sh'
                        sh './deploy.sh $BETA $NAMESPACE staging'
                        sh 'rm kubeconfig.conf service-account.json'
                        }
                    }
                }
                stage('Deploy to production') {
                    when {
                        tag "v*"
                    }
                    environment {
                        VERSION = "${env.TAG_NAME}"
                        NAMESPACE="bbone-prod-1902"
                    }
                    steps {
                        withCredentials([file(credentialsId: '51945fcd-483e-4468-b734-b92b4eb7a288', variable: 'kubeconfig')]) {
                        sh "cp $kubeconfig kubeconfig.conf"
                        sh "chmod 644 kubeconfig.conf"
                        sh "gcloud auth activate-service-account --key-file service-account.json"
                        sh 'chmod +x build.sh'
                        sh './build.sh $VERSION'
                        sh 'chmod +x deploy.sh'
                        sh './deploy.sh $VERSION $NAMESPACE default'
                        sh 'rm kubeconfig.conf service-account.json'
                        }

                    }
                }
            }
        }
    }
    post {
        success {
            office365ConnectorSend webhookUrl: "$TEAMS_MICROSOFT",
                message: "Application logistic Service $SERVICE has been [deployed]",
                color: "05b222",
                status: 'Success'
        }
        failure {
            office365ConnectorSend webhookUrl: "$TEAMS_MICROSOFT",
                message: "Application logistic Service $SERVICE has been [Failed]",
                color: "d00000",
                status: 'Failed'
        }
    }

}
