package main

import (
	"github.com/joho/godotenv"
	"log"
	"git.bluebird.id/fajri.rahmat/sample-grpc/internal/server"
)

func init() {
	godotenv.Load()
}

func main() {
	srv, err := server.New()
	if err != nil {
		log.Fatal(err)
	}
	srv.Run()
}

